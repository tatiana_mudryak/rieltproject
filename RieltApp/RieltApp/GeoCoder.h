//
//  GeoCoder.h
//  RieltApp
//
//  Created by Mudryak on 03.06.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeoCoder : NSObject

+ (void)geoCodingFromAddress:(NSDictionary *)address
                    success:(void (^)(NSDictionary *info))success
                    failure:(void (^)())failure;

+ (void)reverseGeoCodingLatitude:(double)lat longitude:(double)lon
                         success:(void (^)(NSString *address))success
                         failure:(void (^)())failure;

@end
