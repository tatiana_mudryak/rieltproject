//
//  Canvas.h
//  RieltApp
//
//  Created by Mudryak on 04.06.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YandexMapKit.h"

@protocol CanvasDrawingDelegate <NSObject>
//- (void)finishDrawing:(NSMutableArray *)points;
@end

@interface Canvas : UIImageView

@property (nonatomic, strong) YMKMapView *mapView;
@property (nonatomic, strong) NSMutableArray *wayPoints;
@property (nonatomic, strong) NSMutableArray *wayCoordinates;
@property (assign) id <CanvasDrawingDelegate> delegate;

@property (nonatomic) BOOL closePath;
-(void)clearCanvas;
@end
