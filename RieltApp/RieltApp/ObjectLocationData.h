//
//  ObjectLocationData.h
//  RieltApp
//
//  Created by Mudryak on 03.06.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ObjectLocationData : NSObject

@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *email;

@end
