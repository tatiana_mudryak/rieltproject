//
//  GeoCoder.m
//  RieltApp
//
//  Created by Mudryak on 03.06.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "GeoCoder.h"

@implementation GeoCoder

//return address from lat/long
+(void)reverseGeoCodingLatitude:(double)lat longitude:(double)lon
                         success:(void (^)(NSString *address))success
                         failure:(void (^)())failure
{
    
    NSString *str = [NSString stringWithFormat:@"http://geocode-maps.yandex.ru/1.x/?geocode=%f,%f&format=json&kind=house&results=1", lon, lat];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:10.0];
	[request addValue:@"text/json" forHTTPHeaderField:@"Content-Type"];
	[request setHTTPMethod:@"GET"];
	[NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (!connectionError) {
                                   NSError *error;
                                   NSDictionary *responseDict =  [NSJSONSerialization JSONObjectWithData: data
                                                                                                 options: NSJSONReadingAllowFragments
                                                                                                   error: &error];
                                   NSArray *objects = responseDict[@"response"][@"GeoObjectCollection"][@"featureMember"];
                                   if (objects.count > 0) {
                                       success(objects[0][@"GeoObject"][@"name"]);
                                   } else  {
                                       failure();
                                   }
                               } else
                                   NSLog(@"%@", connectionError.localizedDescription);
                           }];
}

#define yandex_key  @"cdVoVL6Cm0IIPzxQYdanMyUxud~R2pfOHXtS0QMPOhO-iN-JV8RhGmN2ONMmENZrK1BnFDvC9m~HWsvaRv2z5Q6zGRO4GLoMpNW7o76R5oY="

// return lat/long from address/name
+(void)geoCodingFromAddress:(NSDictionary *)address
                 success:(void (^)(NSDictionary *info))success
                 failure:(void (^)())failure
{  
   NSString *addressLine = [NSString stringWithFormat:@"%@, улица %@, дом %@", address[@"city"], address[@"street"], address[@"house"]];
   addressLine = [addressLine stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   
   NSString *str = [NSString stringWithFormat:@"http://geocode-maps.yandex.ru/1.x/?geocode=%@&format=json&result=1&lang=ru_RU", addressLine];
   NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]
                                                          cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                      timeoutInterval:10.0];
   [request addValue:@"text/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
   [request setHTTPMethod:@"GET"];
   [NSURLConnection sendAsynchronousRequest:request
                                      queue:[NSOperationQueue mainQueue]
                          completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                          if (!connectionError) {
                                       NSError *error;
                                       NSDictionary *responseDict =  [NSJSONSerialization JSONObjectWithData: data
                                                                                                     options: NSJSONReadingAllowFragments
                                                                                                       error: &error];
                                       NSArray *objects = responseDict[@"response"][@"GeoObjectCollection"][@"featureMember"];
                                       if (objects.count > 0) {
                                           //return string with long/lat ("21.55 65.64")
                                           success(@{@"location" : objects[0][@"GeoObject"][@"Point"][@"pos"],
                                                     @"address" : objects[0][@"GeoObject"][@"name"]});
                                       } else  {
                                           failure();
                                       }
                                   } else
                                       NSLog(@"%@", connectionError.localizedDescription);
                               }];
}



//Поиск объектов в заданной области
//
//Если в запросе указать область поиска, то первыми будут выведены объекты, наиболее близкие к этой области, например: http://geocode-maps.yandex.ru/1.x/?geocode=Ивановка&ll=37.618920,55.756994&spn=3.552069,2.400552.

@end
