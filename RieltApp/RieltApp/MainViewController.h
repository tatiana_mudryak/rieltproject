//
//  MainViewController.h
//  RieltApp
//
//  Created by Mudryak on 03.06.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreData+MagicalRecord.h"
#import "YandexMapKit.h"

@interface MainViewController : UIViewController

@property (nonatomic, strong) IBOutlet YMKMapView *mapView;
@property (nonatomic, strong) NSMutableArray *wayCoordinates;


@end
