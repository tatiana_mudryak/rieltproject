//
//  Canvas.m
//  RieltApp
//
//  Created by Mudryak on 04.06.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "Canvas.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

@interface Canvas ()
{
    CGContextRef context;
    CGPoint startPoint;
    CGPoint currentPoint;
    CGPoint lastPoint;
}

@end
@implementation Canvas


-(NSMutableArray *)wayCoordinates
{
    if (!_wayCoordinates) _wayCoordinates = [[NSMutableArray alloc] init];
    return _wayCoordinates;
}

-(NSMutableArray *)wayPoints
{
    if (!_wayPoints) _wayPoints = [[NSMutableArray alloc] init];
    return _wayPoints;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    currentPoint = [touch locationInView:self];
    
    [self.wayPoints addObject:[NSValue valueWithCGPoint:currentPoint]]; //point in screen coordinates
    YMKMapCoordinate point = [self.mapView convertMapViewPointToLL:currentPoint];
    [self.wayCoordinates addObject:[NSValue valueWithYMKMapCoordinate:point]]; //point in geo coordinates
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint currentLocation = [touch locationInView:self];
    
    YMKMapCoordinate point = [self.mapView convertMapViewPointToLL:currentLocation];
    [self.wayCoordinates addObject:[NSValue valueWithYMKMapCoordinate:point]];
    [self.wayPoints addObject:[NSValue valueWithCGPoint:currentLocation]]; //point in screen coordinates
    NSLog(@"%@", NSStringFromCGPoint(currentLocation));
    [self.layer setNeedsDisplay];
}


-(void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
    CGPoint point;
    CGMutablePathRef path = CGPathCreateMutable();
  
    UIImage *image = [UIImage imageNamed:@"point"];
    CGImageRef imageRef = image.CGImage;
    
    CGContextSetStrokeColorWithColor(ctx, UIColorFromRGB(0xff8000).CGColor);
    CGContextSetLineWidth(ctx, 3.0f);
    CGContextSetLineJoin(ctx, kCGLineJoinBevel);
    
   
    if (self.closePath) {
        CGPathMoveToPoint(path, NULL, currentPoint.x, currentPoint.y);
        CGPathAddLineToPoint(path, NULL, startPoint.x, startPoint.y);
    }
    
    for (int i = 0; i < self.wayCoordinates.count; i++) {
        YMKMapCoordinate coordinate = [[self.wayCoordinates objectAtIndex:i] YMKMapCoordinateValue];
        point = [self.mapView convertLLToMapView:coordinate];
        
        if (i == 0) {
            CGPathMoveToPoint(path, NULL, point.x, point.y);
            CGContextDrawImage(ctx, CGRectMake(point.x-5, point.y-5, 10, 10), imageRef);
            currentPoint = point;
            startPoint = point;
        } else {
            CGPathMoveToPoint(path, NULL, currentPoint.x, currentPoint.y);
            CGPathAddLineToPoint(path, NULL, point.x, point.y);
            currentPoint = point;
            //CGContextStrokePath(ctx);
        }
        CGContextAddPath(ctx, path);
        CGContextDrawPath(ctx, kCGPathStroke);
    }
}

-(void)clearCanvas
{
    [self.wayPoints removeAllObjects];
    [self.wayCoordinates removeAllObjects];
    [self.layer setNeedsDisplay];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"%lu", (unsigned long)self.wayCoordinates.count);
}




@end
