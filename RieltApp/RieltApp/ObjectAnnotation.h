//
//  ObjectAnnotation.h
//  RieltApp
//
//  Created by Mudryak on 03.06.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YandexMapKit.h"

@interface ObjectAnnotation : NSObject <YMKAnnotation>

+(id)objectAnnotation;

@property (assign, nonatomic) YMKMapCoordinate coordinate;
@property (assign, nonatomic) NSString *title;
@property (assign, nonatomic) NSString *subtitle;

@end
