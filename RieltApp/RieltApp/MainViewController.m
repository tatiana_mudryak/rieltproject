//
//  MainViewController.m
//  RieltApp
//
//  Created by Mudryak on 03.06.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "MainViewController.h"
#import "ObjectLocationData.h"
#import "ObjectAnnotation.h"
#import "CalloutContentView.h"
#import "GeoCoder.h"
#import "Canvas.h"

@interface MainViewController () <YMKMapViewDelegate, CanvasDrawingDelegate>

@property (nonatomic) BOOL drawMode;
@property (nonatomic, strong) ObjectLocationData *aboutData;
@property (nonatomic, strong) ObjectAnnotation *annotation;

@property (nonatomic, strong) IBOutlet Canvas *canvas;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *submitDrawingButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *drawButton;

@property (nonatomic, strong) NSMutableArray *coordinatesForSearsh;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.aboutData = [[ObjectLocationData alloc] init];
    
    [YMKConfiguration sharedInstance].apiKey = @"cdVoVL6Cm0IIPzxQYdanMyUxud~R2pfOHXtS0QMPOhO-iN-JV8RhGmN2ONMmENZrK1BnFDvC9m~HWsvaRv2z5Q6zGRO4GLoMpNW7o76R5oY=";
    [self configureMapView];
    
    [self.canvas setFrame:self.mapView.frame];
    self.canvas.delegate = self;
    self.canvas.mapView = self.mapView;

}

-(NSMutableArray *)wayCoordinates
{
    if (_wayCoordinates) _wayCoordinates = [[NSMutableArray alloc] init];
    return _wayCoordinates;
}

#pragma mark - YMKMapViewDelegate Protocol

- (void)configureMapView
{
    self.mapView.showsUserLocation = NO;
    self.mapView.showTraffic = NO;
    self.mapView.delegate = self;
    [self.mapView setCenterCoordinate:YMKMapCoordinateMake(45.044569, 38.964621)
                          atZoomLevel:13
                             animated:NO];
}

-(void)configureAndInstallAnnotation
{
    [self.mapView removeAnnotation:self.annotation];
    self.annotation = [ObjectAnnotation objectAnnotation];
    self.annotation.coordinate = YMKMapCoordinateMake(self.aboutData.location.coordinate.latitude, self.aboutData.location.coordinate.longitude);
    self.annotation.title = self.aboutData.address;
    self.annotation.subtitle = @".....";
    [self.mapView addAnnotation:self.annotation];
    self.mapView.selectedAnnotation = self.annotation;
}

- (YMKCalloutView *)mapView:(YMKMapView *)aMapView calloutViewForAnnotation:(id<YMKAnnotation>)anAnnotation
{
	YMKCalloutView *calloutView = nil;
	
	calloutView = [self.mapView dequeueReusableCalloutViewWithIdentifier:@"calloutViewID"];
	
	if (calloutView == nil)
	{
		calloutView = [[YMKCalloutView alloc] initWithReuseIdentifier:@"calloutViewID"];

		CalloutContentView *contentView = [[CalloutContentView alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
		contentView.label.textAlignment = NSTextAlignmentCenter;
		contentView.label.backgroundColor = [UIColor clearColor];

        calloutView.contentView = contentView;
	}
	
    CalloutContentView *contentView = (CalloutContentView *)calloutView.contentView;
	ObjectAnnotation *annotation = anAnnotation;
	
	contentView.label.text = annotation.title;
	
	return calloutView;
}

-(void)mapView:(YMKMapView *)mapView gotSingleTapAtCoordinate:(YMKMapCoordinate)coordinate
{
 //   if (!drawMode) {
        self.aboutData.location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
        [GeoCoder reverseGeoCodingLatitude:coordinate.latitude
                             longitude:coordinate.longitude
                               success:^(NSString *address) {
                                   self.aboutData.address = address;
                                   [self configureAndInstallAnnotation];
                               } failure:^{
                                   
                               }];
//    } else {
//        CGPoint point = [self.mapView convertLLToMapView:coordinate];
//        [self.drawLayer.pointsCoordinate addObject:[NSValue valueWithYMKMapCoordinate:coordinate]];
//        [self.drawLayer.points addObject:[NSValue valueWithCGPoint:point]];
//        [self.drawLayer setNeedsDisplay];
//    }
}

-(void)mapViewWasDragged:(YMKMapView *)mapView
{
   [self.canvas.layer setNeedsDisplayInRect:self.mapView.bounds];
}

-(void)mapView:(YMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
   [self.canvas.layer setNeedsDisplayInRect:self.mapView.bounds];
}

#pragma mark - Drawing

- (IBAction)setDrawingMode:(id)sender
{
    self.drawMode = !self.drawMode;
    self.mapView.userInteractionEnabled = !self.drawMode;
}

-(void)setDrawMode:(BOOL)drawMode
{
    _drawMode = drawMode;
    if (drawMode) {
        self.drawButton.title = @"Карта";
        [self.mapView removeAnnotation:self.annotation];
        self.canvas.closePath = NO;
        self.canvas.userInteractionEnabled = YES;
    } else {
        self.drawButton.title = @"Рисование";
        self.mapView.userInteractionEnabled = YES;
        self.canvas.userInteractionEnabled = NO;
    }

}

#pragma mark - CanvasDrawing delegate

- (IBAction)searchObjectsInSelectedArea:(id)sender
{
    //close path
    self.canvas.closePath = YES;
    [self.canvas setNeedsDisplay];
    
    //save points
    self.wayCoordinates = self.canvas.wayCoordinates;
    
    //clear canvas
    self.drawMode = NO;
    [self.canvas clearCanvas];
    self.canvas.userInteractionEnabled = NO;
    self.mapView.userInteractionEnabled = YES;
    
    // [self detectObjects];
}

#pragma mark - Searsh objects in specified coordinates area

-(void)detectObjects
{
    YMKMapCoordinate loc = [[self.coordinatesForSearsh objectAtIndex:58] YMKMapCoordinateValue];
    [GeoCoder reverseGeoCodingLatitude:loc.latitude
                             longitude:loc.longitude
                               success:^(NSString *address) {
                                   self.aboutData.location = [[CLLocation alloc] initWithLatitude:loc.latitude longitude:loc.longitude];
                                   self.aboutData.address = address;
                                   [self configureAndInstallAnnotation];
                               } failure:^{
                                   
                               }];
}

@end
