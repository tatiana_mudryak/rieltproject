//
//  CalloutContentView.h
//  RieltApp
//
//  Created by Mudryak on 03.06.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "YMKCalloutContentView-Protocol.h"

@interface CalloutContentView : UIView <YMKCalloutContentView>

@property (nonatomic, readonly) UILabel *label;

@end
