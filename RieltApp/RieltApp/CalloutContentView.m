//
//  CalloutContentView.m
//  RieltApp
//
//  Created by Mudryak on 03.06.14.
//  Copyright (c) 2014 SMediaLink. All rights reserved.
//

#import "CalloutContentView.h"

@interface CalloutContentView ()

@property (nonatomic, readwrite) UILabel *label;

@end

@implementation CalloutContentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
		self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
		[self addSubview:self.label];
    }
    return self;
}


@end
